import java.util.LinkedHashMap;
import java.util.Map;

public class QueryDto {
    private Map<String,Object> map;

    public Map<String,Object> getMap() {
        return map;
    }

    public void setMap(Map<String,Object> map) {
        this.map = map;
    }
}
